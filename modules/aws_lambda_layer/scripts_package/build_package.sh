#!/bin/bash
#https://docs.aws.amazon.com/lambda/latest/dg/python-package.html

rm shared_lambda_package.zip
mkdir package

poetry export -f requirements.txt --output requirements.txt
poetry run pip install -r requirements.txt --target=./package --use-deprecated=legacy-resolver

# shellcheck disable=SC2164
cd package
zip -r ../shared_lambda_package.zip .

# shellcheck disable=SC2103
cd ..
zip shared_lambda_package.zip script.sh
rm -r package
