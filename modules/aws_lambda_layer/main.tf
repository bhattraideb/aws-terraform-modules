resource "aws_lambda_layer_version" "ami_bake_lambda_layer" {
  filename         = "${path.module}/scripts_package/shared_lambda_package.zip"
  source_code_hash = filebase64sha256("${path.module}/scripts_package/shared_lambda_package.zip")     # data.archive_file.python_lambda_package.output_base64sha256
  layer_name       = "ami-bake-lambda-layer"

  compatible_runtimes = ["python3.11"]
}