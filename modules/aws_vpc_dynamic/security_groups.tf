#############################################
########### Public Security Groups ##########
#############################################
resource "aws_security_group" "public" {
  name        = "terraform-${var.environment}-public-sg"
  description = "Public internet Access"
  vpc_id      = aws_vpc.tf_module_vpc.id

  tags = {
    Name = "terraform-${var.environment}-public-sg"
  }
}

resource "aws_security_group_rule" "public_out" {
  type        = "egress"
  from_port   = 0
  to_port     = 0
  protocol    = "-1"
  cidr_blocks = ["0.0.0.0/0"]

  security_group_id = aws_security_group.public.id
}

resource "aws_security_group_rule" "public_in_ssh" {
  type              = "ingress"
  from_port         = 22
  to_port           = 22
  protocol          = "tcp"
  security_group_id = aws_security_group.public.id
  cidr_blocks       = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "public_in_http" {
  type              = "ingress"
  from_port         = 80
  to_port           = 80
  protocol          = "tcp"
  security_group_id = aws_security_group.public.id
  cidr_blocks       = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "public_in_https" {
  type              = "ingress"
  from_port         = 443
  to_port           = 443
  protocol          = "tcp"
  security_group_id = aws_security_group.public.id
  cidr_blocks       = ["0.0.0.0/0"]
}

#############################################
########### Private Security Groups ##########
#############################################
resource "aws_security_group" "private" {
  name        = "terraform-${var.environment}-private-sg"
  description = "Private internet Access"
  vpc_id      = aws_vpc.tf_module_vpc.id

  tags = {
    Name = "terraform-${var.environment}-private-sg"
  }
}

resource "aws_security_group_rule" "private_out" {
  type        = "egress"
  from_port   = 0
  to_port     = 0
  protocol    = "-1"
  cidr_blocks = ["0.0.0.0/0"]

  security_group_id = aws_security_group.private.id
}

resource "aws_security_group_rule" "private_in" {
  type              = "ingress"
  from_port         = 0
  to_port           = 65535
  protocol          = "-1"
  security_group_id = aws_security_group.private.id
  cidr_blocks       = [aws_vpc.tf_module_vpc.cidr_block]
}

#resource "aws_security_group" "tf_access_rds" {
#  vpc_id      = aws_vpc.tf_main.id
#  name        = "allow-postgres"
#  description = "allow-postgres"
#  ingress {
#    from_port       = 5432
#    to_port         = 5432
#    protocol        = "tcp"
#    security_groups = [aws_security_group.tf_allo_ssh.id] # allowing access from our instance
#  }
#  egress {
#    from_port   = 0
#    to_port     = 0
#    protocol    = "-1"
#    cidr_blocks = ["0.0.0.0/0"]
#    self        = true
#  }
#  tags = {
#    Name = "allow-postgres"
#  }
#}
#
#resource "aws_security_group" "tf_elb_security_group" {
#  vpc_id      = aws_vpc.tf_main.id
#  name        = "tf-elb"
#  description = "security group for load balancer"
#  egress {
#    from_port   = 0
#    to_port     = 0
#    protocol    = "-1"
#    cidr_blocks = ["0.0.0.0/0"]
#  }
#
#  ingress {
#    from_port   = 80
#    to_port     = 80
#    protocol    = "tcp"
#    cidr_blocks = ["0.0.0.0/0"]
#  }
#  tags = {
#    Name = "tf-elb"
#  }
#}
#
#resource "aws_security_group" "tf_elastic_beanstalk_app_prod" {
#  vpc_id      = aws_vpc.tf_main.id
#  name        = "TF application - production"
#  description = "security group for my app"
#  egress {
#    from_port   = 0
#    to_port     = 0
#    protocol    = "-1"
#    cidr_blocks = ["0.0.0.0/0"]
#  }
#
#  ingress {
#    from_port   = 22
#    to_port     = 22
#    protocol    = "tcp"
#    cidr_blocks = ["0.0.0.0/0"]
#  }
#
#  tags = {
#    Name = "tf-beanstalk-my-instance"
#  }
#}