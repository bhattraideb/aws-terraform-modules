#######################################
########## Internet Gateways ##########
#######################################
resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.tf_module_vpc.id

  tags = {
    Name = "terraform-${var.environment}-igw"
    VPC  = aws_vpc.tf_module_vpc.id
  }
}

#######################################
############ NAT Gateways #############
#######################################
resource "aws_eip" "nat" {
#  vpc = true
  domain = "vpc"
  #  lifecycle {
  #    prevent_destroy = false
  #  }

  tags = {
    Name = "terraform-${var.environment}-eip"
    VPC  = aws_vpc.tf_module_vpc.id
  }
}

resource "aws_nat_gateway" "ngw" {
  allocation_id = aws_eip.nat.id

  # Whichever the first public subnet happens to be (because NGW needs to be on a public subnet with IGW)
  subnet_id = aws_subnet.public[element(keys(aws_subnet.public), 0)].id

  tags = {
    Name = "terraform-${var.environment}-ngw"
    VPC  = aws_vpc.tf_module_vpc.id
  }
}

#####################################################
############ Route Tables and Routes ################
#####################################################

# Public route table(Subnet with IGW)
resource "aws_route_table" "public" {
  vpc_id = aws_vpc.tf_module_vpc.id

  tags = {
    Name = "terraform-${var.environment}-public-route"
    VPC  = aws_vpc.tf_module_vpc.id
  }
}

# Private route table(Subnet with NGW)
resource "aws_route_table" "private" {
  vpc_id = aws_vpc.tf_module_vpc.id

  tags = {
    Name = "terraform-${var.environment}-private-route"
    VPC  = aws_vpc.tf_module_vpc.id
  }
}

# Public Route
resource "aws_route" "public" {
  route_table_id         = aws_route_table.public.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.igw.id
}

# Private Route
resource "aws_route" "private" {
  route_table_id         = aws_route_table.private.id
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id         = aws_nat_gateway.ngw.id
}

#####################################################
############ Route table association ################
#####################################################

# Public Route to Public Route Table for Public Subnets
resource "aws_route_table_association" "public" {
  for_each       = aws_subnet.public
  subnet_id      = aws_subnet.public[each.key].id
  route_table_id = aws_route_table.public.id
}

# Private Route to Private Route Table for Private Subnets
resource "aws_route_table_association" "private" {
  for_each       = aws_subnet.private
  subnet_id      = aws_subnet.private[each.key].id
  route_table_id = aws_route_table.private.id
}