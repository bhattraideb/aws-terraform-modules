variable "environment" {
  type        = string
  description = "Infrastructure description"
  default     = "Development"
}

variable "vpc_cidr" {
  type        = string
  description = "The IP range to use for VPC"
  default     = "10.0.0.0/16"
}

variable "public_subnet_numbers" {
  type        = map(number)
  description = "Map of AZ to a number that should be used for public subnets"
  default = {
    "us-east-1a" = 1
    "us-east-1b" = 2
#    "us-east-1c" = 3
#    "us-east-1d" = 4
#    "us-east-1e" = 5
  }
}

variable "private_subnet_numbers" {
  type        = map(number)
  description = "Map of AZ to a number that should be used for private subnets"
  default = {
    "us-east-1a" = 6
    "us-east-1b" = 7
#    "us-east-1c" = 8
#    "us-east-1d" = 9
#    "us-east-1e" = 10
  }
}


variable "infra_role" {
  type        = string
  description = "Infrastructure purpose"
  default     = "Developer"
}

variable "instance_size" {
  type        = string
  description = "ec2 web service size"
  default     = "t2.micro"
}

#variable "instance_ami" {
#  type = string
#  description = "Server image to use"
#}

#variable "instance_root_device_size" {
#  type        = number
#  description = "Root bloc size in GB"
#  default     = 12
#}