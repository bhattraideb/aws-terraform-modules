output "vpc_id" {
  value = aws_vpc.tf_module_vpc.id
}

output "vpc_cidr" {
  value = aws_vpc.tf_module_vpc.cidr_block
}

output "vpc_public_subnets" {
  # Result is a map of subnet ID to cidr block e.g. {"subnet_1234" => "10.0.0.1.0/4", ....}
  value = {
    for subnet in aws_subnet.public :
    subnet.id => subnet.cidr_block
  }
}


output "vpc_private_subnets" {
  # Result is a map of subnet ID to cidr block e.g. {"subnet_1234" => "10.0.0.1.0/4", ....}
  value = {
    for subnet in aws_subnet.private :
    subnet.id => subnet.cidr_block
  }
}

output "private_subnet_ids" {
  value = [for subnet in aws_subnet.private : subnet.id]
}

output "public_subnet_ids" {
  value = [for subnet in aws_subnet.public : subnet.id]
}

output "security_group_public" {
  value = aws_security_group.public.id
}

output "security_group_private" {
  value = aws_security_group.private.id
}