# Create a VPC for the region associated with the AZ
resource "aws_vpc" "tf_module_vpc" {
  cidr_block = var.vpc_cidr

  tags = {
    Name = "terraform-${var.environment}-vpc"
  }
}

resource "aws_subnet" "public" {
  for_each   = var.public_subnet_numbers
  vpc_id     = aws_vpc.tf_module_vpc.id
  cidr_block = cidrsubnet(aws_vpc.tf_module_vpc.cidr_block, 4, each.value)

  tags = {
    Name   = "terraform-${var.environment}-public-subnet"
    Subnet = "${each.key}-${each.value}"
  }
}

resource "aws_subnet" "private" {
  for_each   = var.private_subnet_numbers
  vpc_id     = aws_vpc.tf_module_vpc.id
  cidr_block = cidrsubnet(aws_vpc.tf_module_vpc.cidr_block, 4, each.value)

  tags = {
    Name   = "terraform-${var.environment}-private-subnet"
    Subnet = "${each.key}-${each.value}"
  }
}