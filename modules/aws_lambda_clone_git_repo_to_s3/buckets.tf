resource "aws_s3_bucket" "packer_repo_bucket" {
  bucket        = var.packer_repo_bucket_name
  force_destroy = true
}

resource "aws_s3_bucket_versioning" "versioning" {
  bucket = aws_s3_bucket.packer_repo_bucket.id

  versioning_configuration {
    status = "Enabled"
  }
}