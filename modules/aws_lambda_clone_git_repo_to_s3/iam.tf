resource "aws_iam_role" "clone_repo_lambda_role" {
  name = "clone-repo-lambda-role"
  assume_role_policy = data.aws_iam_policy_document.lambda_policy.json
}

data "aws_iam_policy_document" "lambda_policy" {
  statement {
    actions = ["sts:AssumeRole"]
    effect = "Allow"
    principals {
      identifiers = ["lambda.amazonaws.com"]
      type        = "Service"
    }
  }
}

data "aws_iam_policy_document" "lambda_ec2_policy" {
  statement {
    sid    = "AllowLambdaToAccessVPC"
    effect = "Allow"

    actions = [
      "ec2:DescribeNetworkInterfaces",
      "ec2:DeleteNetworkInterface",
      "ec2:CreateNetworkInterface",
      "ec2:DetachNetworkInterface",
    ]

    resources = ["*", ]
  }

  statement {
    sid = "AllowLambdaToLogCloudWatch"
    effect = "Allow"

    actions = [
      "logs:CreateLogGroup",
      "logs:CreateLogStream",
      "logs:PutLogEvents"
    ]
    resources = ["*"]
  }

  statement {
    sid = "AllowLambdaToSNS"
    effect = "Allow"

    actions = ["sns:*"]
    resources = ["*"]
  }

}

data "aws_iam_policy" "s3_admin" {
  name = "AmazonS3FullAccess"
}

resource "aws_iam_policy" "ec2_lambda_policy" {
  name   = "ec2-lambda-policy"
  policy = data.aws_iam_policy_document.lambda_ec2_policy.json
}

resource "aws_iam_role_policy_attachment" "ec2_policy_to_lambda_role" {
  policy_arn = aws_iam_policy.ec2_lambda_policy.arn
  role       = aws_iam_role.clone_repo_lambda_role.name
}

resource "aws_iam_role_policy_attachment" "s3_policy_to_lambda_role" {
  policy_arn = data.aws_iam_policy.s3_admin.arn
  role       = aws_iam_role.clone_repo_lambda_role.name
}
