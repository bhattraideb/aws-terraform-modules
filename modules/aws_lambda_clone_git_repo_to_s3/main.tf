module "lambda_layer" {
  source = "../aws_lambda_layer"
}

# data "archive_file" "clone_lambda_zip" {
#   source_file = "${path.module}/lambda_function.py"
#   output_path = "${path.module}/lambda_package.zip"
#   type        = "zip"
# }

resource "aws_lambda_function" "gitlab_to_s3_lambda_function" {
  layers           = [module.lambda_layer.lambda_layer_arn]
  filename         = "${path.module}/lambda_package.zip"
  function_name    = "clone_gitlab_repo_to_s3"
  role             = aws_iam_role.clone_repo_lambda_role.arn
  handler          = "lambda_function.lambda_handler"
 source_code_hash = filebase64sha256("${path.module}/lambda_package.zip")
#   source_code_hash = data.archive_file.clone_lambda_zip.output_base64sha256

  runtime     = "python3.11"
  timeout     = 60 # Inseconds
  memory_size = 2048

  ephemeral_storage {
    size = 2048
  }

  environment {
    variables = {
      foo = "bar"
    }
  }

  vpc_config {
    security_group_ids = [aws_security_group.outbound_access.id] #var.security_group_ids
    subnet_ids         = var.private_subnet_ids
  }
}

resource "aws_lambda_permission" "allow_cloudwatch" {
  statement_id  = "AllowExecutionFromCloudWatch"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.gitlab_to_s3_lambda_function.function_name
  principal     = "events.amazonaws.com"
  source_arn    = aws_cloudwatch_event_rule.repo_pull_schedule.arn
#  qualifier = aws_lambda_alias.s3_lambda_function_alias.name
}