variable "vpc_id" {
  type        = string
  description = "VPC ID for lambda SG"
}

variable "private_subnet_ids" {
  type        = list(string)
  description = "Subnet IDs for lambda"
}

variable "environment" {
  type        = string
  description = "Infrastructure description"
  default     = "Development"
}

variable "packer_repo_bucket_name" {
  type        = string
  description = "S3 bucket name where code will be cloned from external repo"
}



