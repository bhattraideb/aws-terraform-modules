output "clone_repo_lambda_arn" {
  value = aws_lambda_function.gitlab_to_s3_lambda_function.arn
}

output "clone_repo_lambda_function_name" {
  value = aws_lambda_function.gitlab_to_s3_lambda_function.function_name
}

output "cloudwatch_event_rule_name" {
  value = aws_cloudwatch_event_rule.repo_pull_schedule.name
}

output "outbound_access_sg_id" {
  value = aws_security_group.outbound_access.id
}

output "packer_repo_bucket" {
  value = aws_s3_bucket.packer_repo_bucket.bucket
}

output "packer_repo_bucket_arn" {
  value = aws_s3_bucket.packer_repo_bucket.arn
}

output "packer_repo_bucket_id" {
  value = aws_s3_bucket.packer_repo_bucket.id
}
