resource "aws_cloudwatch_event_rule" "repo_pull_schedule" {
  name                = "trigger-packer-repo-pull"
  description         = "Checks Git repo every 5 minutes for new commits"
  schedule_expression = "rate(5 minutes)" #"rate(1 hour)"
}

resource "aws_cloudwatch_event_target" "lambda-function-target" {
  target_id = "lambda-function-target"
  rule      = aws_cloudwatch_event_rule.repo_pull_schedule.name
  arn       = aws_lambda_function.gitlab_to_s3_lambda_function.arn
}

resource "aws_lambda_permission" "allow_event_bridge_to_trigger_lambda" {
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.gitlab_to_s3_lambda_function.function_name
  principal     = "events.amazonaws.com"
}