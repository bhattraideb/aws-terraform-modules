from dulwich import porcelain
import os
import shutil
import tempfile
import boto3
from botocore.errorfactory import ClientError
import logging


# GITHUB_REPO_URL = "https://github.com/bhattraideb/aws-ami-packer.git"
GITLAB_REPO_URL = "https://gitlab.com/bhattraideb/aws-ami-packer.git"
BUCKET_NAME = "packer-repo-bucket"
OBJECT_KEY = "ami-code-package.zip"


def lambda_handler(event, context):
    try:
        logging.info('Cloning packer repo from  Gitlab..')
        latest_commit_ref = get_latest_commit_hash_from_main()
        if latest_commit_ref == get_latest_commit_ref_from_s3_object():
            return f"Nothing to push, latest version of git repo already exists in S3. Latest Commit Ref: {latest_commit_ref}"
        file_name = clone_repo()
        repo_file_name = zip_repo(file_name)
        response = push_repo_to_s3(latest_commit_ref, repo_file_name)
        return f"New version of repo pushed to S3. Commit Ref: {latest_commit_ref}"

    except Exception as e:
        logging.error(f'Exception:\n{e}')

def get_latest_commit_hash_from_main():
    refs = porcelain.ls_remote(GITLAB_REPO_URL)
    return refs[b"refs/heads/main"].decode('utf8')

def get_latest_commit_ref_from_s3_object():
    s3_resource = boto3.resource('s3')
    try:
        metadata = s3_resource.meta.client.get_object_tagging(Bucket=BUCKET_NAME, Key=OBJECT_KEY)
    except ClientError as e:
        if e.response["Error"]["Code"] == "NoSuchKey":
            print("No object found - continuing")
            return
        else:
            raise e

    for tag in metadata["TagSet"]:
        if tag["Key"] == "commit_head":
            return tag["Value"]
    raise Exception("No commit ref found for this object")


def clone_repo():
    local_clone_dir = tempfile.TemporaryDirectory('-packer-repo').name
    packer_subdir = os.path.join(local_clone_dir, 'packer-repo')
    os.makedirs(packer_subdir)
    repo = porcelain.clone(GITLAB_REPO_URL, target=packer_subdir)
    return packer_subdir


def zip_repo(filename):
    shutil.make_archive(f"{filename}/repo","zip", filename)
    if "repo.zip" not in os.listdir(filename):
        raise Exception("Zipped folder not found")
    return f"{filename}/repo.zip"


def push_repo_to_s3(commit_head, filename):
    try:
        s3_resource = boto3.resource('s3')
        s3_resource.meta.client.upload_file(filename, BUCKET_NAME, OBJECT_KEY)
        s3_resource.meta.client.put_object_tagging(
            Bucket=BUCKET_NAME,
            Key=OBJECT_KEY,
            Tagging={
                "TagSet": [{"Key": "commit_head", "Value": commit_head}]
            }
        )
    except Exception as err:
        print("exp: ", err)
        raise
    return "File Uploaded Successfully"

