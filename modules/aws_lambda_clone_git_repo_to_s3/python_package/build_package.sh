#!/bin/bash
#pip install --upgrade pip
mkdir package
ls
poetry export -f requirements.txt --output requirements.txt
poetry run pip install -r requirements.txt --target=./package --use-deprecated=legacy-resolver


#shellcheck disable=SC2164
cd package
ls
zip -r ../lambda_package.zip .
# shellcheck disable=SC2103
cd ..
ls

zip lambda_package.zip lambda_function.py
mv zip lambda_package.zip ../
rm -r package
rm lambda_package.zip
rm requirements.txt
