data "aws_ami" "terraform_app_ami" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  filter {
    name   = "root-device-type"
    values = ["ebs"]
  }

  owners = ["099720109477"]
}

resource "aws_instance" "terraform_web" {
  ami           = data.aws_ami.terraform_app_ami.id
  instance_type = var.instance_type

  root_block_device {
    volume_size = "20"
    volume_type = "gp3"
  }

  subnet_id              = var.subnet_id
  vpc_security_group_ids = [aws_security_group.public_access.id]

  tags = {
    Name = var.ec2_name
  }
}

#resource "aws_eip_association" "eip_association" {
#  count         = (var.create_eip) ? 1 : 0
#  instance_id   = aws_instance.terraform_web.id
#  allocation_id = module.aws_custom_vpc.elastic_ip_id
#}
