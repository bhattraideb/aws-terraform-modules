output "instance_ip" {
  value = aws_instance.terraform_web.public_ip
}

output "instance_id" {
  value = aws_instance.terraform_web.id
}

output "instance_name" {
  value = aws_instance.terraform_web.tags.Name
}

#output "app_elastic_ip" {
#  value = aws_eip.eip_address.*.public_ip
#}

#output "app_elastic_ip" {
#  value = aws_eip.terraform_addr.public_ip
#}
