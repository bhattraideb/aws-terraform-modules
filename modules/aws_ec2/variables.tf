variable "ec2_name" {
  type        = string
  description = "EC2 name for tagging purpose"
}

variable "instance_security_group_name" {
  type        = string
  description = "Security group for EC2 to be launched"
}

variable "subnet_id" {
  type        = string
  description = "Subnet ID where EC2 have to be launched"
}

variable "instance_type" {
  type        = string
  description = "ec2 web service size"
}

variable "create_eip" {
  type        = bool
  default     = false
  description = "Whether to create an EIP for EC2 instance or not"
}

variable "vpc_id" {
  type        = string
  description = "VPC ID where EC2 will be launched"
}



