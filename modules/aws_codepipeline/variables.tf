variable "operating_system" {
  description = "Name of the OS for which AMI need to build"
  type = string
  default = "Amazon-Linux2023"
}

variable "code_pipeline_name" {
  type        = string
  description = "Codepipeline name"
}

variable "get_source_stage_name" {
  type        = string
  description = "Pipeline stage for downloading code from S3"
}

variable "build_project_name" {
  type        = string
  description = "CodeBuild project name"
}

variable "test_project_name" {
  type        = string
  description = "Codebuild project name for testing AMI"
}

variable "deregister_project_name" {
  type        = string
  description = "Codebuild project name for testing AMI"
}

variable "source_code_bucket" {
  description = "S3 bucket which stores packer repo as zip file"
  type        = string
}

variable "packer_repo_bucket_arn" {
  description = "S3 bucket which stores packer repo as zip file"
  type        = string
}

variable "source_code_bucket_key" {
  description = "Source code bucket for packer repo as zip file"
  type        = string
}

#variable "pipeline_artifact_bucket" {
#  description = "S3 bucket where pipeline artifacts will be stored"
#  type        = string
#}

#variable "ami_distribute_name" {
#  description = "Share Amazon Linux2"
#  type        = string
#}
#
#variable "ami_deregister_name" {
#  type        = string
#  description = "S3 bucket to store pipeline artifact"
#}


