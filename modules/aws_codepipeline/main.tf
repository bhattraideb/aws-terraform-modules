resource "aws_codepipeline" "ami_creation_pipeline" {
  name     = var.code_pipeline_name
  role_arn = aws_iam_role.code_pipeline_role.arn

  artifact_store {
    location = var.source_code_bucket
    type     = "S3"

    #    encryption_key {
    #      id   = data.aws_kms_alias.s3kmskey.arn
    #      type = "KMS"
    #    }
  }

  stage {
    name = var.get_source_stage_name

    action {
      name     = var.get_source_stage_name
      category = "Source"
      owner    = "AWS"
      provider = "S3"
      version  = "1"
#            output_artifacts = ["${var.pipeline_artifact_bucket_id}/source-output",]
      output_artifacts = ["Source", ]

      configuration = {
        PollForSourceChanges = false
        S3Bucket             = var.source_code_bucket
        S3ObjectKey          = var.source_code_bucket_key
      }
    }
  }

  stage {
    name = "${var.operating_system}-Build"

    action {
      name     = "${var.operating_system}-Build"
      category = "Build"
      owner    = "AWS"
      provider = "CodeBuild"
      #      input_artifacts  = ["${var.pipeline_artifact_bucket_id}/source-output",]
      #      output_artifacts = ["${var.pipeline_artifact_bucket_id}/build-output",]
      input_artifacts  = ["Source", ]
#      output_artifacts = ["Build", ]
      version          = "1"

      configuration = {
        ProjectName = var.build_project_name
      }
    }
  }

  stage {
    name = "${var.operating_system}-Test"

    action {
      name     = "${var.operating_system}-Test"
      category = "Build"
      owner    = "AWS"
      provider = "CodeBuild"
      #      input_artifacts = ["${var.pipeline_artifact_bucket_id}/build-output",]
      input_artifacts = ["Source", ]
      version         = "1"

      configuration = {
        ProjectName = var.test_project_name
        #        ActionMode     = "REPLACE_ON_FAILURE"
        #        Capabilities   = "CAPABILITY_AUTO_EXPAND,CAPABILITY_IAM"
        #        OutputFileName = "CreateStackOutput.json"
        #        StackName      = "MyStack"
        #        TemplatePath   = "build_output::sam-templated.yaml"
      }
    }
  }

#  stage {
#    name = var.ami_distribute_name
#
#    action {
#      name     = var.ami_distribute_name
#      category = "Build"
#      owner    = "AWS"
#      provider = "CodeBuild"
#      #      input_artifacts = ["${var.pipeline_artifact_bucket_id}/build-output"]
#      input_artifacts = ["Source", ]
#      version         = "1"
#
#      configuration = {
#        ProjectName = var.ami_distribute_name
#      }
#    }
#  }
#
 stage {
   name = "${var.operating_system}-Deregister-Old-AMIs"

   action {
     name     = "${var.operating_system}-Deregister-Old-AMIs"
     category = "Build"
     owner    = "AWS"
     provider = "CodeBuild"
     #      input_artifacts = ["${var.pipeline_artifact_bucket_id}/build-output"]
     input_artifacts = ["Source"]
     version         = "1"

     configuration = {
       ProjectName = var.deregister_project_name
     }
   }
 }
}