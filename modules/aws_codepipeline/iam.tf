resource "aws_iam_role" "code_pipeline_role" {
  name               = "bakery-pipeline-role"
  assume_role_policy = data.aws_iam_policy_document.pipeline_policy_document.json
}

resource "aws_iam_policy" "codepipeline_policy" {
  name   = "codepipeline-policy"
  policy = data.aws_iam_policy_document.pipeline_permission.json
}

resource "aws_iam_role_policy_attachment" "codepipeline_policy_attachment" {
  role       = aws_iam_role.code_pipeline_role.name
  policy_arn = aws_iam_policy.codepipeline_policy.arn
}

data "aws_iam_policy_document" "pipeline_policy_document" {
  statement {
    actions = ["sts:AssumeRole"]
    effect  = "Allow"

    principals {
      type        = "Service"
      identifiers = ["codepipeline.amazonaws.com"]
    }

  }
}

data "aws_iam_policy_document" "pipeline_permission" {
  statement {
    sid    = "AllowCodePipelineToAMIs"
    effect = "Allow"

    actions = [
      "logs:*",
      "s3:*"
    ]

    resources = ["*"]
#    resources = ["${var.packer_repo_bucket_arn}/*"]
  }

  statement {
    sid    = "AllowCodePipelineToAccessCodeBuildJobs"
    effect = "Allow"

    actions = [
      "codebuild:StartBuild",
      "codebuild:StopBuild",
      "codebuild:RetryBuild",
      "codebuild:List*",
      "codebuild:BatchGetBuilds",
    ]
    resources = ["*"]
  }
}

