output "pipeline_artifact_bucket" {
  value = aws_s3_bucket.pipeline_artifact.bucket
}

output "pipeline_artifact_bucket_id" {
  value = aws_s3_bucket.pipeline_artifact.id
}
