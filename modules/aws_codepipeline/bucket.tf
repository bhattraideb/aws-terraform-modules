resource "aws_s3_bucket" "pipeline_artifact" {
  bucket        = "tf-codepipeline-artifact"
  force_destroy = true
}
