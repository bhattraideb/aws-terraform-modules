output "eks_cluster_name" {
  value = aws_eks_cluster.api-cluster.name
}

output "endpoint" {
  value = aws_eks_cluster.api-cluster.endpoint
}

output "kubeconfig-certificate-authority-data" {
  value = aws_eks_cluster.api-cluster.certificate_authority[0].data
}