variable "eks_cluster_role_name" {
  type        = string
  description = "Role name for EKS"
}

variable eks_cluster_name {
  type        = string
  description = "Cluster name"
}

variable subnet_ids {
  type        = list(string)
  description = "Public subnet IDs of cluster"
  default = ["ChangeMe", "ChangeMe"]
}
