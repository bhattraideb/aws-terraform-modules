resource "aws_iam_role" "codebuild_role" {
  name               = "bakery_codebuild_role"
  assume_role_policy = data.aws_iam_policy_document.codebuild_trust_relationship.json
}


resource "aws_iam_policy" "codebuild_policy" {
  name = "bakery_codebuild_policy"
  policy = data.aws_iam_policy_document.codebuild_permissions.json
}


resource "aws_iam_role_policy_attachment" "codebuild_role_policy_attachment" {
  policy_arn = aws_iam_policy.codebuild_policy.arn
  role       = aws_iam_role.codebuild_role.name
}

resource "aws_iam_instance_profile" "packer_instance_profile" {
  name = "packer_instance_profile"
  role = aws_iam_role.packer_instance_profile_role.name
}

resource "aws_iam_role" "packer_instance_profile_role" {
  name = "packer_instance_profile_role"
  assume_role_policy = data.aws_iam_policy_document.packer_ec2_role_trust_policy.json
}

resource "aws_iam_role_policy_attachment" "packer_instance_profile_policy_attachment" {
  policy_arn = aws_iam_policy.packer_instance_profile_policy.arn
  role       = aws_iam_role.packer_instance_profile_role.name
}

resource "aws_iam_policy" "packer_instance_profile_policy" {
  name   = "packer_instance_profile_policy"
  policy = data.aws_iam_policy_document.packer_instance_profile_policy_document.json
}

data "aws_iam_policy" "ssm_full_access_policy" {
#   https://docs.aws.amazon.com/aws-managed-policy/latest/reference/AmazonSSMFullAccess.html
#   name = "AmazonSSMFullAccess"
  arn = "arn:aws:iam::aws:policy/AmazonSSMFullAccess"
}

resource "aws_iam_role_policy_attachment" "ssm_policy_attachment" {
  policy_arn = data.aws_iam_policy.ssm_full_access_policy.arn
  role       = aws_iam_role.packer_instance_profile_role.name
}
