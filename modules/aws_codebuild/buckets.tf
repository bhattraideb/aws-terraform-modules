resource "aws_s3_bucket" "pipeline_artifact_bucket" {
  bucket        = var.pipeline_artifact_bucket
  force_destroy = true
}

resource "aws_s3_bucket_versioning" "versioning" {
  bucket = aws_s3_bucket.pipeline_artifact_bucket.id

  versioning_configuration {
    status = "Enabled"
  }
}