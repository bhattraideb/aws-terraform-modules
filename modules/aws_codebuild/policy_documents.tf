data "aws_iam_policy_document" "codebuild_trust_relationship" {
  statement {
    effect = "Allow"
    actions = ["sts:AssumeRole"]
    principals {
      type        = "Service"
      identifiers = ["codebuild.amazonaws.com"]
    }
  }
  statement {
    effect = "Allow"
    actions = ["sts:AssumeRole"]
    principals {
      type        = "AWS"
      identifiers = ["*"]  # Need to restrict this
    }
  }
}

data "aws_iam_policy_document" "codebuild_permissions" {
  statement {
    sid = "AllowCodeBuildToBuildAmis"
    effect = "Allow"

    actions = [
      "ssm:*",
      "secretmanager:GetSecretValue",
      "s3:*",     # may not be required this
      "logs:*",
      "inspector:*",
      "iam:*",      # may not be required this
      "ec2:*",
#      "ecr:BatchCheckLayerAvailability",
#      "ecr:BatchGetImage",
#      "ecr:GetDownloadUrlForLayer"
    ]

    resources = ["*"]
  }

  statement {
    sid = "AccessECRImagesForCOdeBuildJobs"
    effect = "Allow"

    actions = [
      "ecr:BatchGetImage",
    ]

    resources = [var.linux2_ecr_repo_arn]
  }
}

data "aws_iam_policy_document" "packer_ec2_role_trust_policy" {
  statement {
    sid = "AllowEC2InstanceToAssumeThisRole"
    effect = "Allow"
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ec2.amazonaws.com"]
    }
  }
}

data "aws_iam_policy_document" "packer_instance_profile_policy_document" {
  statement {
    sid    = "AllowPackerToAccessSecrets"
    effect = "Allow"

    actions = [
      "secretmanager:GetSecretValue",
      "kms:Decrypt"
    ]

    resources = ["*"]
  }
}
