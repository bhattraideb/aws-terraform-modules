variable "vpc_id" {
  type        = string
  description = "VPC ID for CodePipeline"
}

variable "codebuild_name" {
  description = "Codebuild name"
  type        = string
}

variable "codebuild_acceptance_test_name" {
  description = "Acceptance test name for Linux2 AMI"
  type        = string
}

variable "codebuild_deregister_name" {
  description = "Build name for de-registering old Linux2 AMIs"
  type        = string
}

variable "private_subnet_ids" {
  type        = list(string)
  description = "Subnet IDs for Codebuild"
}

variable "linux2_image_url" {
  type        = string
  description = "ECR Repository URL for Amazon Linux2"
}

variable "linux2_ecr_repo_arn" {
  type        = string
  description = "ECR Repository ARN for Amazon Linux2"
}


variable "pipeline_artifact_bucket" {
  type        = string
  description = "Bucket to store pipelinr artifacts"
}
