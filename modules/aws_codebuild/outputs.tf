output "pipeline_artifact_bucket" {
  value = aws_s3_bucket.pipeline_artifact_bucket.bucket
}

output "codebuild_name" {
  value = aws_codebuild_project.build_amazon_linux2023_ami.name
}

output "codebuild_acceptance_test_name" {
  value = aws_codebuild_project.test_amazon_linux2023_ami.name
}

output "codebuild_deregister_name" {
  value = aws_codebuild_project.deregister_amazon_linux2023_ami.name
}


#output "pipeline_artifact_bucket" {
#  value = aws_s3_bucket.pipeline_artifact_bucket.bucket
#}
