resource "aws_codebuild_project" "build_amazon_linux2023_ami" {
  name          = var.codebuild_name
  description   = "CodeBuild name for specific AMI"
  build_timeout = 5
  service_role  = aws_iam_role.codebuild_role.arn

  artifacts {
    type = "NO_ARTIFACTS"
  }

#  cache {
#    type     = "S3"
#    location = aws_s3_bucket.example.bucket
#  }

  environment {
    compute_type                = "BUILD_GENERAL1_SMALL"
    image                       = var.linux2_image_url    # "aws/codebuild/amazonlinux2-x86_64-standard:4.0"
    type                        = "LINUX_CONTAINER"
    image_pull_credentials_type = "CODEBUILD"    #"SERVICE_ROLE"

#    environment_variable {
#      name  = "HTTP_PROXY"
#      value = "http://proxy.vpclocal:8000"
#    }
#
#    environment_variable {
#      name  = "HTTPS_PROXY"
#      value = "http://proxy.vpclocal:8000"
#    }
#
#    environment_variable {
#      name  = "http_proxy"
#      value = "http://proxy.vpclocal:8000"
#    }
#
#    environment_variable {
#      name  = "https_proxy"
#      value = "http://proxy.vpclocal:8000"
#    }
  }

#  logs_config {
#    cloudwatch_logs {
#      group_name  = "log-group"
#      stream_name = "log-stream"
#    }
#
#    s3_logs {
#      status   = "ENABLED"
#      location = "${var.source_code_bucket_id}/build-log"
##      location = "${aws_s3_bucket.example.id}/build-log"
#    }
#  }

#  source {
#    type            = "GITHUB"
#    location        = "https://github.com/mitchellh/packer.git"
#    git_clone_depth = 1
#
#    git_submodules_config {
#      fetch_submodules = true
#    }
#  }

  source {
    type      = "NO_SOURCE"
    buildspec = file("${path.module}/static/build_amazon_linux2023_ami.yml")
  }

  vpc_config {
    vpc_id             = var.vpc_id
    subnets            = var.private_subnet_ids
    security_group_ids = [aws_security_group.codebuild_outbound_access.id]
  }
  source_version = "master"
}

resource "aws_codebuild_project" "test_amazon_linux2023_ami" {
  name          = var.codebuild_acceptance_test_name
  description   = "Acceptance test for specific AMI build"
  build_timeout = 5
  service_role  = aws_iam_role.codebuild_role.arn

  artifacts {
    type = "NO_ARTIFACTS"
  }

  environment {
    compute_type                = "BUILD_GENERAL1_SMALL"
    image                       = var.linux2_image_url    # "aws/codebuild/amazonlinux2-x86_64-standard:4.0"
    type                        = "LINUX_CONTAINER"
    image_pull_credentials_type = "CODEBUILD"    #"SERVICE_ROLE"

#    environment_variable {
#      name  = "HTTP_PROXY"
#      value = "http://proxy.vpclocal:8000"
#    }
#
#    environment_variable {
#      name  = "HTTPS_PROXY"
#      value = "http://proxy.vpclocal:8000"
#    }
#
#    environment_variable {
#      name  = "http_proxy"
#      value = "http://proxy.vpclocal:8000"
#    }
#
#    environment_variable {
#      name  = "https_proxy"
#      value = "http://proxy.vpclocal:8000"
#    }
  }

  source {
    type      = "NO_SOURCE"
    buildspec = file("${path.module}/static/test_amazon_linux2023_ami.yml")
  }

  vpc_config {
    vpc_id             = var.vpc_id
    subnets            = var.private_subnet_ids
    security_group_ids = [aws_security_group.codebuild_outbound_access.id]
  }
  source_version = "master"
}

#resource "aws_codebuild_project" "share_amazon_linux2023_ami" {
#  name          = var.codebuild_name
#  description   = "Share Amazon Linux2 AMI"
#  build_timeout = 5
#  service_role  = aws_iam_role.codebuild_role.arn
#
#  artifacts {
#    type = "NO_ARTIFACTS"
#  }
#
#  environment {
#    compute_type                = "BUILD_GENERAL1_SMALL"
#    image                       = var.linux2_image_url    # "aws/codebuild/amazonlinux2-x86_64-standard:4.0"
#    type                        = "LINUX_CONTAINER"
#    image_pull_credentials_type = "CODEBUILD"    #"SERVICE_ROLE"
#
#    environment_variable {
#      name  = "HTTP_PROXY"
#      value = "http://proxy.vpclocal:8000"
#    }
#
#    environment_variable {
#      name  = "HTTPS_PROXY"
#      value = "http://proxy.vpclocal:8000"
#    }
#
#    environment_variable {
#      name  = "http_proxy"
#      value = "http://proxy.vpclocal:8000"
#    }
#
#    environment_variable {
#      name  = "https_proxy"
#      value = "http://proxy.vpclocal:8000"
#    }
#  }
#
#  source {
#    type      = "NO_SOURCE"
#    buildspec = file("${path.module}/static/linux2_ami_acceptance_test.yml")
#  }
#
#  vpc_config {
#    vpc_id             = var.vpc_id
#    subnets            = var.private_subnet_ids
#    security_group_ids = [aws_security_group.codebuild_outbound_access.id]
#  }
#  source_version = "master"
#}

resource "aws_codebuild_project" "deregister_amazon_linux2023_ami" {
 name          = var.codebuild_deregister_name
 description   = "Deregister Amazon Linux2 AMI"
 build_timeout = 5
 service_role  = aws_iam_role.codebuild_role.arn

 artifacts {
   type = "NO_ARTIFACTS"
 }

 environment {
   compute_type                = "BUILD_GENERAL1_SMALL"
   image                       = var.linux2_image_url    # "aws/codebuild/amazonlinux2-x86_64-standard:4.0"
   type                        = "LINUX_CONTAINER"
   image_pull_credentials_type = "CODEBUILD"    #"SERVICE_ROLE"

#    environment_variable {
#      name  = "HTTP_PROXY"
#      value = "http://proxy.vpclocal:8000"
#    }
#
#    environment_variable {
#      name  = "HTTPS_PROXY"
#      value = "http://proxy.vpclocal:8000"
#    }
#
#    environment_variable {
#      name  = "http_proxy"
#      value = "http://proxy.vpclocal:8000"
#    }
#
#    environment_variable {
#      name  = "https_proxy"
#      value = "http://proxy.vpclocal:8000"
#    }
 }

 source {
   type      = "NO_SOURCE"
   buildspec = file("${path.module}/static/deregister_linux_2023_ami.yml")
 }

 vpc_config {
   vpc_id             = var.vpc_id
   subnets            = var.private_subnet_ids
   security_group_ids = [aws_security_group.codebuild_outbound_access.id]
 }
 source_version = "master"
}