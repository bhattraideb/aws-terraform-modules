resource "aws_security_group" "codebuild_outbound_access" {
  vpc_id      = var.vpc_id
  name        = "codebuild_outbound_access"
  description = "Security group for Lambda function outbound access"

  egress {
    from_port = 0
    to_port   = 0
    protocol  = -1
    #    cidr_blocks = [var.MY_LOCAL_IP]
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "codebuild_outbound_access"
  }
}