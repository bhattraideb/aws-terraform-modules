variable "environment" {
  type        = string
  description = "Infrastructure description"
  default     = "Development"
}
