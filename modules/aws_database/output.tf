output "rds_endpoint" {
  value = aws_db_instance.rds.endpoint
}
output "rds_name" {
  value = aws_db_instance.rds.db_name
}
output "rds_address" {
  value = aws_db_instance.rds.address
}
output "rds_arn" {
  value = aws_db_instance.rds.arn
}
output "rds_port" {
  value = aws_db_instance.rds.port
}
output "rds_username" {
  value = aws_db_instance.rds.username
}
output "rds_security_group_id" {
  value = aws_security_group.rds.id
}
output "rds_db_identifier" {
  value = aws_db_instance.rds.identifier
}
output "rds_db_id" {
  value = aws_db_instance.rds.id
}
output "rds_host" {
  value = aws_db_instance.rds.domain
}