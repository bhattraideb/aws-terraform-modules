resource "aws_db_instance" "rds" {
  allocated_storage      = var.storage
  max_allocated_storage  = var.max_allocated_storage
  engine                 = var.engine
  engine_version         = var.engine_version
  instance_class         = var.instance_type
  identifier             = var.db_identifier_name
  db_name                = var.database_name
  username               = var.username
  password               = var.password
#   db_subnet_group_name   = var.subnet_group != "" ? var.subnet_group : aws_db_subnet_group.rds[0].name
  db_subnet_group_name   = aws_db_subnet_group.rds.name
  parameter_group_name   = aws_db_parameter_group.rds.name
  multi_az               = var.multi_az
  vpc_security_group_ids = [aws_security_group.rds.id]

  storage_type                        = var.storage_type
  backup_retention_period             = var.backup_retention_period
  skip_final_snapshot                 = true      # Should be false for production
  final_snapshot_identifier           = "${var.db_identifier_name}-final-snapshot"
  iam_database_authentication_enabled = var.iam_database_authentication_enabled
  storage_encrypted                   = var.encryption_at_rest ? true : false
  kms_key_id                          = var.encryption_at_rest ? aws_kms_alias.rds[0].target_key_arn : ""

  deletion_protection          = var.deletion_protection
  performance_insights_enabled = var.performance_insight_enabled

  tags = {
    Name = var.db_identifier_name
  }
}

resource "aws_db_subnet_group" "rds" {
#   count       = var.subnet_group != "" ? 0 : 1
  name        = var.db_identifier_name
  description = "${var.db_identifier_name} subnet"
  subnet_ids  = var.subnet_ids
}

resource "aws_db_parameter_group" "rds" {
  name        = var.db_identifier_name
  family      = var.engine_family
  description = "rds parameter group for ${var.db_identifier_name}"

  dynamic "parameter" {
    for_each = var.parameters
    content {
      name  = parameter.value.name
      value = parameter.value.value
    }
  }
}

