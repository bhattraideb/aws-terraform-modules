variable "storage" {
  description = "RDS storage in GB"
}
variable "max_allocated_storage" {
  description = "If greater than storage it will enable storage autoscaling"
}
variable "engine" {
  description = "RDS engine"
}
variable "engine_version" {
  description = "RDS engine version"
}
variable "instance_type" {
  description = "RDS instance type"
}
variable "db_identifier_name" {
  description = "RDS instance name"
}
variable "database_name" {
  description = "RDS database name"
}
variable "username" {
  description = "RDS username"
}
variable "password" {
  description = "Password for the DB"
}
# variable "subnet_group" {
#   description = "subnet group to launch RDS in"
#   type        = list(string)
#   default     = []
# }
variable "multi_az" {
  description = "if true, enable multi-az deployment"
}
variable "storage_type" {
  description = "RDS storage type"
}
variable "backup_retention_period" {
  description = "RDS backup retention period"
}
variable "iam_database_authentication_enabled" {
  description = "enable iam auth"
  default     = true
}
variable "deletion_protection" {
  description = "Enable Deletion Protection"
}
variable "encryption_at_rest" {
  description = "enable at rest encryption with KMS"
}
variable "performance_insight_enabled" {
  description = "Enable Performance Insight"
}
variable "engine_family" {
  description = "RDS engine family"
}
variable "vpc_id" {
  description = " vpc id"
}
variable "subnet_ids" {
  description = "subnet ids to launch RDS in"
}
variable "parameters" {
  description = "rds parameters to set"
  default     = []
  type = list(object({
    name  = string
    value = string
  }))
}
variable "ingress_security_groups" {
  description = "Security groups to allow"
}
variable "allow_self" {
  description = "if true, allows traffic from self"
  default     = false
}
