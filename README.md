# AWS Terraform Modules
## Build Python package
https://python-poetry.org/docs/cli/
- `poetry new scripts_package`
- `cd scripts_package`
- `update pyproject.toml for dependencies like:`
    - `python = "^3.10"`
    - `boto3 = "~1.33.7"`
    - `dulwich = "~0.21.6"`
- `poetry export -f requirements.txt --output requirements.txt`
- `poetry run pip install -r requirements.txt --target=.package --use-deprecated=legacy-resolver`
